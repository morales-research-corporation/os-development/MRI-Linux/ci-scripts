#
# SPDX-License-Identifier: GPL-3.0-or-later

SCRIPTS=$(wildcard scripts/*.sh)
YAML_FILES=$(wildcard *.yml .*.yml *.yaml)

all:

check: shellcheck shfmt yamllint

shellcheck:
	shellcheck -s bash $(SCRIPTS)

shfmt:
	shfmt -i 2 -ci -d $(SCRIPTS)

yamllint:
	yamllint $(YAML_FILES)

.PHONY: check shellcheck shfmt yamllint
